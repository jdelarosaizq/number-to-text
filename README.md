# ROULETTE #

## How do I get set up? ##

* Check if you have Java 8 and Maven 3.
* Clone the repo
* Run `mvn verify`

## How I run the tests? ##

* Same above

## How I run the app? ##

From the project folder:

* `$ cd target/classes`

* `$ java com.jdelarosa.NumberToText 12345678


## Dependencies ##

There are no runtime dependencies, just for testing

[INFO] --- maven-dependency-plugin:2.1:tree (default-cli) @ search ---

[INFO] com.jdelarosa:search:jar:1.0.0

[INFO] +- junit:junit:jar:4.11:test