package com.jdelarosa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnitTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }


    @Test(expected = IllegalArgumentException.class)
    public void testParseUnit_moreThan19_() throws Exception {
        new Unit().parse(20);
    }

    @Test
    public void testParseUnit_lessThan20_() throws Exception {
        assertEquals("zero", new Unit().parse(0));
        assertEquals("nineteen", new Unit().parse(19));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseNegativeUnit_throwException() throws Exception {
        new Unit().parse(-19);
    }

} 
