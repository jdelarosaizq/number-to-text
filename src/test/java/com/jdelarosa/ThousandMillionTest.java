package com.jdelarosa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThousandMillionTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test(expected = IllegalArgumentException.class)
    public void NaN_should_throwException() throws Exception {
        new ThousandMillion("NaN");
    }

    @Test()
    public void test_small_number() throws Exception {
        assertEquals("zero", new ThousandMillion(0).toString());
        assertEquals("fifty", new ThousandMillion(50).toString());
        assertEquals("one", new ThousandMillion(1).toString());
        assertEquals("one hundred", new ThousandMillion(100).toString());
    }

    @Test()
    public void test_big_number() throws Exception {

        assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty one",
                new ThousandMillion(56945781).toString());

        assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty two",
                new ThousandMillion(56945782).toString());

        // 1.056.945.782
        assertEquals("one thousand fifty six million nine hundred and forty five thousand seven hundred and eighty two",
                new ThousandMillion(1056945782).toString());

        // 6.945.782
        assertEquals("six million nine hundred and forty five thousand seven hundred and eighty two",
                new ThousandMillion(6945782).toString());

        // 945.782
        assertEquals("nine hundred and forty five thousand seven hundred and eighty two",
                new ThousandMillion(945782).toString());

        assertEquals("forty five thousand seven hundred and eighty two",
                new ThousandMillion(45782).toString());

        assertEquals("five thousand seven hundred and eighty two",
                new ThousandMillion(5782).toString());

        assertEquals("seven hundred and eighty two",
                new ThousandMillion(782).toString());

        assertEquals("eighty two",
                new ThousandMillion(82).toString());
    }

    @Test()
    public void edges() throws Exception {

        assertEquals("zero", new ThousandMillion(0).toString());

        //999.999.999
        assertEquals("nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine",
                new ThousandMillion(999999999).toString());

        //2,147,483,647
        assertEquals("two thousand one hundred and forty seven million four hundred and eighty three thousand six hundred and forty seven",
                new ThousandMillion(Integer.MAX_VALUE).toString());

        //1.000.000.000
        assertEquals("one hundred million and one",
                new ThousandMillion(100000001).toString());

        //1.001.000
        assertEquals("one million one thousand",
                new ThousandMillion(1001000).toString());
    }

} 
