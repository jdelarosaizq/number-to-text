package com.jdelarosa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HundredTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }


    @Test
    public void testParseHundred_lessThan100() throws Exception {
        assertEquals("zero", new Hundred().parse(0));
        assertEquals("ninety nine", new Hundred().parse(99));
    }

    @Test
    public void testParseHundred_moreThan99_lessThan999() throws Exception {
        assertEquals("one hundred", new Hundred().parse(100));
        assertEquals("nine hundred and ninety", new Hundred().parse(990));
        assertEquals("nine hundred and ninety nine", new Hundred().parse(999));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseNegativeTen_throwException() throws Exception {
        new Hundred().parse(-200);
    }

}
