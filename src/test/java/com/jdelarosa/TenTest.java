package com.jdelarosa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TenTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }


    @Test
    public void testParseTen_lessThan20() throws Exception {
        assertEquals("zero", new Ten().parse(0));
        assertEquals("one", new Ten().parse(1));
        assertEquals("ten", new Ten().parse(10));
        assertEquals("nineteen", new Ten().parse(19));
    }

    @Test
    public void testParseTen_moreThan19_lessThan99() throws Exception {
        assertEquals("twenty", new Ten().parse(20));
        assertEquals("ninety nine", new Ten().parse(99));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseNegativeTen_throwException() throws Exception {
        new Ten().parse(-20);
    }

}
