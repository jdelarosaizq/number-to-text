package com.jdelarosa;

/**
 * Hello world!
 */
public class NumberToText {

    public static void main(String[] args) {

        try {

            if (args == null || args.length != 1) {
                System.out.println("Error. Numeric parameter expected.");
                System.exit(0);
            }

            ThousandMillion num = new ThousandMillion(args[0]);
            System.out.println(num);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}
