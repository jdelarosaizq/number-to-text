package com.jdelarosa;

public class Ten extends Unit {

    private static final String[] tens = {
            "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"
    };


    @Override
    protected String parse(int num) {

        if (num > 99 || num < 0) {
            throw new IllegalArgumentException("Invalid number. The input should be an integer number between 0 and 99");
        }

        final int ten = num / 10;
        final int units = num % 10;
        if (ten > 1) {
            return tens[ten] + (units > 0 ? " " + super.parse(num % 10) : "");
        }
        return super.parse(num);
    }

}
