package com.jdelarosa;

public class Unit {

    private static final String[] units = {
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
            "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
    };

    protected String parse(int num) {

        if (num > 19 || num < 0) {
            throw new IllegalArgumentException("Invalid number. The input should be a number between 0 and 19");
        }

        return units[num];
    }
}
