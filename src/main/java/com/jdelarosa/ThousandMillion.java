package com.jdelarosa;

public class ThousandMillion extends Hundred {

    private int value;

    public ThousandMillion(String num) {

        try {
            this.value = Integer.valueOf(num);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid number. The input should be an integer number between 0 and 2,147,483,647");
        }
    }

    public ThousandMillion(int num) {
        this.value = num;
    }

    @Override
    public String toString() {
        return parse(value);
    }

    @Override
    protected String parse(int value) {

        final String num = String.valueOf(value);
        int rest = num.length() % 3;
        int decimalPoints = num.length() / 3;

        if (rest == 0) {
            rest = 3;
        } else {
            decimalPoints++;
        }

        final String leftDigits = num.substring(0, rest);
        final String rightDigits = num.substring(rest, num.length());

        /* First process the left side of the most left decimal point and then the rest */
        String output = super.parse(Integer.valueOf(leftDigits));
        output += parseDecimalPoints(decimalPoints, rightDigits);

        return output;
    }

    private String parseDecimalPoints(int decimalPoints, String num) {

        String output = "";
        int index = 0;
        for (int i = decimalPoints - 1; i > 0; i--) {

            /* The number can be just "thousand million", "million" or "thousand" */
            if (i == 2) {
                output += " million";
            } else if (!output.trim().endsWith("million")) {
                output += " thousand";
            }

            /* Each group of 3 digits are hundred. English way use "and" for final tens and units" */
            String digits = num.substring(index, index + 3);
            int hundred = Integer.valueOf(digits);
            if (hundred > 0) {
                if (hundred < 100 && i == 1) {
                    output += " and";
                }
                output += " " + super.parse(hundred);
            }

            index += 3;
        }
        return output;
    }

}
