package com.jdelarosa;

public class Hundred extends Ten {

    static final String suffix = " hundred";

    @Override
    protected String parse(int num) {

        if (num > 999 || num < 0) {
            throw new IllegalArgumentException("Invalid number. The input should be a number between 0 and 999");
        }

        final int hundreds = num / 100;
        final int tens = num - hundreds * 100;

        if (hundreds > 0) {
            return super.parse(hundreds) + suffix + (tens > 0 ? " and " + super.parse(tens) : "");
        }

        return super.parse(num);
    }
}
